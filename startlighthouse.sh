#!/bin/sh

env

# Beacon Node
mkdir -p /data/lighthouse
exec /usr/sbin/lighthouse bn --checkpoint-sync-url-timeout 300 \
                     --checkpoint-sync-url $CHECKPOINT \
                     --execution-endpoint http://localhost:8551 \
                     --execution-jwt /tmp/jwtsecret \
                     --disable-deposit-contract-sync \
                     --network mainnet \
                     --datadir /data/lighthouse \
                     --http
