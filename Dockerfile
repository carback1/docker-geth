FROM alpine:latest
LABEL maintainer "Richard T. Carback III <rick@xx.network>"

RUN apk update --no-cache
RUN apk upgrade --no-cache

##
# Compile dependencies first
##

# Compile and install geth
# NOTE: When updating, be sure to select the last VERIFIED build.
RUN apk add git go make linux-headers --no-cache && \
    git clone --branch v1.14.3 --depth 1 https://github.com/ethereum/go-ethereum && \
    cd go-ethereum && make all && cp -r build/bin/* /usr/sbin/ && cd .. && \
    apk del git go make linux-headers  --no-cache  && \
    rm -fr go-ethereum /root/go /root/.cache

# Compile and install lighthouse
# https://users.rust-lang.org/t/unable-to-find-libclang-the-libclang-issue/70746
ENV LIBCLANG_PATH="/usr/lib/llvm17/lib"
# https://github.com/rust-lang/wg-cargo-std-aware/issues/70
# https://users.rust-lang.org/t/dynamic-linking-in-alpine/105608
ENV RUSTFLAGS="-Ctarget-feature=-crt-static -Lnative=/usr/lib"
RUN apk add build-base git go make cmake clang clang-dev perl linux-headers --no-cache && \
    wget https://sh.rustup.rs && mv index.html rust.sh && chmod +x rust.sh && \
    ./rust.sh -y && \
    git clone --branch stable --depth 1 https://github.com/sigp/lighthouse.git && \
    cd lighthouse && PATH="/root/.cargo/bin:$PATH" make && cd .. && \
    mv lighthouse/target/release/lighthouse /usr/sbin/ && \
    rm -fr lighthouse /root/.cargo /root/.cache /root/.rustup && \
    apk del build-base git go make cmake clang clang-dev perl linux-headers --no-cache


##
# Environment vars and disk setup for this image
##

# The domain name this image will use for letsencrypt certs
ENV DNSNAME=""
# The e-mail address this image will use for letsencrypt certs
ENV EMAIL=""
# The callback url for letsencrypt. This will be POST'd at
# with the DNS settings to use.
ENV CERTBOT_CALLBACK=""
# An optional auth token you can include, if you ever do
# any kind of automation on the callback URL instead of manual DNS
ENV CERTBOT_AUTHTOKEN="none"
# The default checkpoint server to use, full list here:
# https://eth-clients.github.io/checkpoint-sync-endpoints/
ENV CHECKPOINT="https://beaconstate.ethstaker.cc/"

# Map out our data directories
RUN mkdir /data
RUN mkdir /data/letsencrypt && ln -s /data/letsencrypt /etc/letsencrypt
RUN mv /var/log /data/logs && ln -s /data/logs /var/log
RUN mkdir /data/ethereum && ln -s /data/ethereum /root/.ethereum


##
# Service installation and setup
##

# Geth
EXPOSE 30303
# NOTE: We could but we do not expose these, 8546 is exposed via nginx
#       reverse proxy
#EXPOSE 8545
#EXPOSE 8546
#EXPOSE 8547

# Nginx and Certbot
RUN apk add nginx certbot certbot-nginx --no-cache
COPY nginx-config.conf /etc/nginx/http.d/default.conf
COPY index.html /var/www/index.html
EXPOSE 80
EXPOSE 443

# DNS dig and curl utilities
RUN apk add bind-tools curl --no-cache
COPY dnswait.py /dnswait.py

# Configure supervisor for running inside a container
RUN apk add supervisor openssl rsync --no-cache
RUN mkdir -p /var/log/supervisor/

COPY supervisord.conf /etc/supervisord.conf
COPY start.sh /start.sh
COPY startgeth.sh /startgeth.sh
COPY startlighthouse.sh /startlighthouse.sh
RUN chmod +x *.sh *.py

STOPSIGNAL SIGQUIT

RUN cp -r /data /data-initial
VOLUME /data
CMD [ "/start.sh" ]
