#!/usr/bin/env python3

import os
import time
import subprocess
import logging
import json

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
log = logging.getLogger(__name__)

# When we're in docker alpine print to default tty output
if os.path.exists("/dev/pts/0"):
    logging.basicConfig(filename="/dev/pts/0", encoding="utf-8",
                        level=logging.DEBUG)


# If set, we will call this with a POST so you can write a script to handle the
# DNS challenge from another machine.
CALLBACK = os.environ.get("CERTBOT_CALLBACK")
AUTHTOKEN = os.environ.get("CERTBOT_AUTHTOKEN", "none")

# CERTBOT Values
DOMAIN = os.environ["CERTBOT_DOMAIN"]
if DOMAIN.startswith("*."):
    DOMAIN = DOMAIN[2:]
VALIDATION_DOMAIN = "_acme-challenge.{}".format(DOMAIN)
VALIDATION_TOKEN = os.environ["CERTBOT_VALIDATION"]

def check_dns(domain, expected_challenge):
    cmd = ["dig", "@1.1.1.1", "txt", domain]
    proc = subprocess.run(cmd, capture_output=True)
    if proc.returncode != 0:
        logging.error("ERROR: {} returned {}: {}".format(cmd, proc.returncode,
                                                         proc))
    if expected_challenge in str(proc.stdout):
        return True
    return False

def run_callback(cb, domain, challenge):
    data = json.dumps({"domain": domain,
                       "challenge": challenge,
                       "token": AUTHTOKEN})
    cmd = ["curl", "-X", "POST",
           "-H", "Content-Type: application/json",
           "--data", data,
           cb]
    logging.info("Callback: {}".format(cmd))
    proc = subprocess.run(cmd, capture_output=True)
    if proc.returncode != 0:
        logging.error("ERROR: {} returned {}: {}".format(cmd, proc.returncode,
                                                         proc))

if __name__ == "__main__":
    while not check_dns(VALIDATION_DOMAIN, VALIDATION_TOKEN):
        logging.info("Please add the following DNS Challenge to txt records:")
        logging.info("\t{}\t{}".format(VALIDATION_DOMAIN, VALIDATION_TOKEN))

        if CALLBACK is not None and CALLBACK != "":
            run_callback(CALLBACK, VALIDATION_DOMAIN, VALIDATION_TOKEN)

        time.sleep(10)
    os._exit(0)
