#!/bin/sh

if [ ! -f /data/.initialized ]
then
    touch /data/.initialized
    rsync -avz /data-initial/ /data/
    echo "Data volume empty, initialized fresh state!"
fi

# Redirect to "syslog" file but keep printing out in this console as well
exec > >(tee -a "/data/syslog") 2>&1

NOTLS="false"

if [ -z "$DNSNAME" ]
then
    echo "You must set DNSNAME to the dns name of the node you are running for tls"
    NOTLS="true"
    exit -1
fi

if [ -z "$EMAIL" ]
then
    echo "You must set EMAIL to the e-mail address for letsencrypt for tls"
    NOTLS="true"
    exit -1
fi

# https://eth-clients.github.io/checkpoint-sync-endpoints/#mainnet
if [ -z "$CHECKPOINT" ]
then
    export CHECKPOINT=https://beaconstate.ethstaker.cc/
fi

env

echo "Starting in 3s..."
sleep 3

# jwt secret
[ ! -s /tmp/jwtsecret ] && openssl rand -hex 32 | tr -d "\n" > /tmp/jwtsecret

if [ "$NOTLS" = "false" ]
then
    if [ ! -d /etc/letsencrypt/live ]
    then
      # HTTPS challenge - this can only work if you have 80/443 access
      #/usr/bin/certbot --nginx -n --agree-tos --no-redirect -m $EMAIL -d $DNSNAME

      # DNS-01 with CERTBOT_CALLBACK via dnswait.py, calls out via POST
      # to to a url that will handle post requests to let you know when it needs
      # an update.
      /usr/bin/certbot certonly --preferred-challenges dns -n \
                 --manual --manual-auth-hook /dnswait.py \
                 --agree-tos -m $EMAIL -d $DNSNAME
    fi

    sed -i "s/localhost/$DNSNAME/g" /etc/nginx/http.d/default.conf
    sed -i "s/localhost/$DNSNAME/g" /var/www/index.html

    /usr/bin/certbot install --nginx -n --no-redirect --cert-name $DNSNAME

    # Add renew to crontab to renew the certs
    echo "0 0,12 * * * root certbot renew --manual-auth-hook /dnswait.py" >> /etc/crontabs/root

    # So certbot does stupid things and starts nginx after install
    # kill it and wait for ports to unbind
    pkill nginx
    sleep 1
    echo "tls enabled"
fi

if [ "$NOTLS" = "true" ]
then
    echo "tls disabled"
fi


echo "Starting supervisord..."
exec /usr/bin/supervisord -c /etc/supervisord.conf
