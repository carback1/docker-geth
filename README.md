# Docker Go-Ethereum RPC Node

This uses a lighthouse beacon node and a geth instance to create a
personal ethereum node that runs on docker.

It also sets up certbot in an intelligent way using dns-01 challenges,
so you can use whatever ports you want.


You can run it with:

```
docker run -e DNSNAME=geth-internal.carback.us -e EMAIL=email@gmail.com \
    -e CERTBOT_CALLBACK=https://example.com/certbot_dns.php \
    -p 31443:443 -p 30303:30303 \
    -v /host/docker-volumes/geth:/data carback1/geth:v0.0.4
```
