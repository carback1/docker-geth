#!/bin/sh

env

# Start go-ethereum
# Note the vhosts/origins settings are to handle port mapping issues.
# Probably not necessary for authrpc since that is always running local but
# for http and ws it is a good idea to accept from everywhere:
# https://ethereum.stackexchange.com/questions/79273/geth-on-aws-ec2-rpc-call-networking#79512
# https://github.com/ethereum/go-ethereum/issues/16526
exec /usr/sbin/geth  --state.scheme=path --cache 8192 --history.transactions 0 \
                --authrpc.addr localhost --authrpc.port 8551 \
                --authrpc.vhosts '*' --authrpc.jwtsecret /tmp/jwtsecret \
                --http --http.vhosts '*' --ws --ws.origins '*'
